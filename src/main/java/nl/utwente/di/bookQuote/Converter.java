package nl.utwente.di.bookQuote;

public class Converter {
    public double convertTemperature(String temparature) throws Exception {
        return Double.parseDouble(temparature) * 1.8 + 32;
    }
}
