import nl.utwente.di.bookQuote.Converter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        Assertions.assertEquals(50.0, converter.convertTemperature("10"));
    }
}
